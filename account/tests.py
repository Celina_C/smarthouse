import unittest
from forms import FormRegister
from django.contrib.auth.models import User
from django.test.client import Client
from account import forms
from django.conf import settings
from django.contrib.auth.models import User
from django.core import mail
from django.core import management
from django.core.urlresolvers import reverse
from django.test import TestCase
import datetime
import sha


class ClientTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()

    def test_details(self):
        response = self.client.get('/register/')

        self.failUnlessEqual(response.status_code, 200)

        # self.failUnlessEqual(len(response.context['customers']),5)


class RegisterUserTest(TestCase):

    def setUp(self):
        self.profile1 = User.objects._create_user(username='testuser', email='testmail', id=1,
                                                  password='testpwd', is_staff=1, is_superuser=1)

        self.profile2 = User.objects._create_user(username='testuser', email='testmail', id=1,
                                                  password='testpwd', is_staff=1, is_superuser=1)
        self.profile1.save()
        self.profile2.save()

    def test_add_new_user(self):
        self.assertEqual(self.profile1.id, 1)
        self.assertEqual(self.profile2.email, 'testmail')
        profile = User.objects.get(pk=1)

    def test_form(self):
        form_data = {'username': 'testuser', 'email': 'testmail', 'id': 1, 'password': 'testpwd', 'is_staff': 1, 'is_superuser': 1}
        form = FormRegister(data=form_data)
        # self.assertEqual(form.is_valid(), True)

    def test_user_is_active(self):
        self.assertIs(self.profile1.is_active, True)
        self.assertIs(self.profile2.is_active, True)

    def test_registration_profile_created(self):
        self.assertEqual(User.objects.count(), 1)

    def test_activation_email(self):
        self.assertEqual(len(mail.outbox), 0)


class RegistrationFormTests(TestCase):

    def test_registration_form(self):

        invalid_data_dicts = [
            {'data': {'username': 'foo/bar', 'email': 'foo@example.com', 'password1': 'foo', 'password2': 'foo'},
                'error': ('username', [u"Enter a valid value."])},
            {'data': {'username': 'alice', 'email': 'alice@example.com', 'password1': 'secret', 'password2': 'secret'},
                'error': ('username', [u"This username is already taken. Please choose another."])},
            {'data': {'username': 'foo', 'email': 'foo@example.com', 'password1': 'foo', 'password2': 'bar'},
                'error': ('__all__', [u"You must type the same password each time"])},
        ]

        for invalid_dict in invalid_data_dicts:
            form = forms.RegistrationForm(data=invalid_dict['data'])
            self.failIf(form.is_valid())
            self.assertEqual(form.errors[invalid_dict['error'][0]], invalid_dict['error'][1])

        form = forms.RegistrationForm(data={'username': 'foo', 'email': 'foo@example.com', 'password1': 'foo',
                                            'password2': 'foo'})
        self.failUnless(form.is_valid())