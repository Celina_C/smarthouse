import unittest
from django.core import mail


class EmailTest(unittest.TestCase):

    def test_send_email(self):
        mail.send_mail('Temat', 'Wiadomosc', 'od@example.com', ['do@example.com'], fail_silently=False)
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(mail.outbox[0].subject, 'Temat')